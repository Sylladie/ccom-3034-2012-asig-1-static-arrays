//--- Program to test List class.

#include <iostream>
#include <cstdlib> // exit(), EXIT_SUCCESS

#include "List.h"

int main()
{
   // Test the class constructor
   List intList1;
   std::cout << "Constructing intList1\n";

   for (int i=0; i<=10; i++) intList1.append( i );

   std::cout<< "Contents for list intList1 after using append( i ): \n";
   intList1.print( std::cout ); std::cout<<"\n";

   List intList2;
   std::cout << "Constructing intList2\n";

   for(int i=0; i<=10; i++) intList2.append( i );

   std::cout<<"Contents for list intList2 after using append( i ): \n";
   intList2.print( std::cout ); std::cout<<"\n";

   std::cout<<"Reversing intList2.\n";
   intList2.reverse( );

   std::cout<<"Contents for list intList2 after using reverse( ): \n";
   intList2.print( std::cout ); std::cout<<"\n";

   std::cout<<"Extending intList1 with intList2.\n";
   intList1.extend( intList2 );

   std::cout<<"Contents for list intList1 after using extend( list ): \n";
   intList1.print( std::cout ); std::cout<<"\n";

   std::cout<< "Inserting 10 10's at position 10 into intList1.\n";
   for(int i=0; i<=10; i++) intList1.insert( 10, 10 );

   std::cout<<"Removing 10 from intList1.\n";
   intList1.remove( 10 );

   std::cout<<"Contents for list intList1 after using remove( x ): \n";
   intList1.print( std::cout ); std::cout<<"\n";

   std::cout<< "The first element of intList1 was "<< intList1.pop( )<< ".\n";

   std::cout<< "The element at position 11 of intList1 was "<< intList1.pop( 11 )<< "\n";

   std::cout<< "There is a 5 at position "<< intList1.index( 5 )<< "\n";

   std::cout<< "There are "<< intList1.count( 10 )<< " 10 's in intList1.\n";

   exit(EXIT_SUCCESS); 
}
