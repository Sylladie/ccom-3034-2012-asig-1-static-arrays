#include <cstdlib> // exit(), EXIT_FAILURE
#include "List.h"

const std::string FULL = "full";
const std::string EMPTY = "empty";
const std::string BOUNDS = "bounds";
const std::string UNFOUND = "!found";


/* TODO: add some sort exception handling. Id est, work out some mechanism to 
         deal with error occuring in fuctions that return a value. What value,
         if any, should I return? 
*/       

List::List(): mySize(0) {}

void List::insert(int pos, ElementType item) {
    if (!full()){
        if (withinBounds(pos)){
            // Shift elements in greater positions to the right
            for (int i=mySize; i>pos; i--){
                myArray[i] = myArray[i-1];
            }
            // Insert new element into list
            myArray[pos] = item;
            // Increase the size of the array
            mySize++;
        } else if (pos == mySize){  // Inserting in the last position of the list
            myArray[pos] = item;
            mySize++;
        } else {
             err_out(BOUNDS);   
        }
    } else {
        err_out(FULL);
    }
}

void List::erase(int pos) {
    if (!empty()){
        if (withinBounds(pos)){
            // Shift all posterior element to the left, erasing the item at position pos
            for (int i=pos; i<mySize-1; i++){
                myArray[i] = myArray[i+1];
            }
            // Decrease the size of the array
            mySize--;
        } else {
            err_out(BOUNDS);
        }
    } else{
        err_out(UNFOUND);
    }
}

void List::remove(ElementType item) {
    if (!empty()){
        int pos = 0;
        while ((myArray[pos]!=item) && pos<=mySize) pos++;
        if (!(pos==mySize)){
            erase(pos);
        } else {
            err_out(UNFOUND);
        }
    } else {
        err_out(EMPTY);
    }
}

ElementType List::pop(int i) {
    ElementType item = myArray[i];
    if (!empty()){
        if (withinBounds(i)){
            erase(i);
        } else {
            err_out(BOUNDS);
        }
    } else {
        err_out(EMPTY);
    }
    return item;
}

ElementType List::pop() {
    ElementType item = myArray[0];
    if (!empty()) {
        erase(0);
    } else {
        err_out(EMPTY);
    }
    return item;
}

void List::append(ElementType item) {
    insert(mySize, item);
}

void List::extend(List & other_list) {
    int otherSize = other_list.getSize();
    if (mySize+otherSize < CAPACITY){
        for (int i=0; i<=otherSize; i++){
            myArray[mySize+i] = other_list.getElemAtPos(i);
        }
        mySize += otherSize;
    } else {
        err_out(FULL);
    }
}

int List::index(ElementType item) const {
    if (!empty()){
        for (int i=0; i<mySize; i++) if (myArray[i] == item) return i;
    } else {
        err_out(EMPTY);
    }
    err_out(UNFOUND);
}

int List::count(ElementType item) const{
    if (!empty()){
        int counter = 0;
        for (int i=0; i<mySize; i++) if (myArray[i] == item) counter++;
        return counter;
    } else {
        return 0;
    }
}

void List::reverse() {
    if (!empty()){
        ElementType temp[mySize];
        int tempSize = mySize;
        for (int i=mySize-1, j=0; i>=0; i--, j++) temp[j] = pop(i);
        for (int i=0; i<tempSize; i++) append(temp[i]);
    } else {
        err_out(EMPTY);
    }
}

bool List::full() const {
    return (mySize == CAPACITY);
}

bool List::empty() const {
    return (mySize == 0);
}

bool List::withinBounds(int pos) const {
    return (pos >= 0 || pos < mySize);
}

void List::display(std::ostream & out) const {
    if (!empty()){
        std::cout<< myArray[0]<< ", ";
        for(int pos=1; pos<mySize-1; pos++){
            std::cout<< myArray[pos]<< ", ";
        }
        std::cout<< myArray[mySize-1];
    } else {
        std::cout<<"";
    }
}

void List::print(std::ostream & out) const {
    out<<"( " <<*this <<" )";   
}

std::ostream & operator<<(std::ostream & out, const List & aList) {
    aList.display(out);
    return out;
}

void err_out(std::string const &error) {
    if (error == "full"){
        std::cerr<<"No space available for new element -- List unchanged\n";
    } else if (error == "empty"){
        std::cerr<< "There are no elements to remove -- List unchanged\n";
    } else if (error == "bounds"){
        std::cerr<< "Delete position is not valid -- List unchanged\n";
    } else if (error == "!found"){
        std::cerr<< "Item was not found in the list -- List unchanged\n";
    }
}