# http://codepad.org/Gos7qiv7 

all: listtester

listtester: listtester.o List.o
	c++ -o listtester listtester.o List.o

listtester.o: listtester.cpp
	c++ -c listtester.cpp
	
List.o: List.cpp
	c++ -c List.cpp

clear:
	rm -f *.o
	
pack:
	tar cvzf CCOM3034-Asig01-JohnGallegos.tgz Makefile *.cpp *.h
