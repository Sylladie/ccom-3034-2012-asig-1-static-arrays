/*-- List.h ---------------------------------------------------------------
 
  This header file defines the data type List for processing lists.
  Basic operations are:
     Constructor
     empty:    Check if list is empty
     insert:   Insert an item
     erase:    Remove an item
     display:  Output the list
     <<:       Output operator
-------------------------------------------------------------------------*/
#include <iostream>
#include <string>  

#ifndef LIST
#define LIST

const int CAPACITY = 1024;
typedef int ElementType;

class List
{
 public: 
   /***** Class constructor *****/
   List();
   /*----------------------------------------------------------------------
     Construct a List object.

     Precondition:  None
     Postcondition: An empty List object has been constructed; mySize is 0.
   -----------------------------------------------------------------------*/

   /***** empty operation *****/
 /********
   bool empty() const;
   ----------------------------------------------------------------------
     Check if a list is empty.

     Precondition:  None
     Postcondition: true is returned if the list is empty, false if not.
   -----------------------------------------------------------------------*/
   
   /***** empty operation *****/
   bool full() const;
   /*----------------------------------------------------------------------
     Check if a list is full.

     Precondition:  None
     Postcondition: true is returned if the list is full, false if not.
   -----------------------------------------------------------------------*/   

   /***** insert and erase *****/
   void insert(int pos, ElementType item);
   /*----------------------------------------------------------------------
     Insert a value into the list at a given position.

     Precondition:  item is the value to be inserted; there is room in
         the array (mySize < CAPACITY); and the position satisfies
         0 <= pos <= mySize. 
     Postcondition: item has been inserted into the list at the position
         determined by pos (provided there is room and pos is a legal
         position).
   -----------------------------------------------------------------------*/

   void erase(int pos);
   /*----------------------------------------------------------------------
     Remove a value from the list at a given position.

     Precondition:  The list is not empty and the position satisfies
         0 <= pos < mySize.
     Postcondition: element at the position determined by pos has been
         removed (provided pos is a legal position).
   ----------------------------------------------------------------------*/

   void remove( ElementType item );
   /*----------------------------------------------------------------------
     Remove removes the first instance of item found in the list.

     Precondition:  The list is not empty and the position satisfies
         0 <= pos < mySize.
     Postcondition: First instance of the element found in the list is 
         removed.
   ----------------------------------------------------------------------*/

   ElementType pop( );
   ElementType pop( int i );
   /*----------------------------------------------------------------------
     pop([i]) where i is an optional parameter. If i is passed then it
     removes and returns the element at position i. If i is not passed
     then it removes and returns the first element of the list.

     Precondition:  The list is not empty and the position satisfies
         0 <= pos < mySize.
     Postcondition: Removes and returns the first element or the element 
          at position i of the list.
   ----------------------------------------------------------------------*/

   
   void append( ElementType item );
   /*----------------------------------------------------------------------
     Inserts a value at the end of the list.

     Precondition:  The list is not full.
     Postcondition: element at the last position of the list 
         (provided that mySize < CAPACITY).
   ----------------------------------------------------------------------*/   
   
    void extend( List &other_list );
   /*----------------------------------------------------------------------
     Inserts the elements of a second list at the list.

     Precondition:  The list is not full.
     Postcondition: The elements of the second list at the last 
         position of the list (provided that mySize < CAPACITY).
   ----------------------------------------------------------------------*/   

   void print(std::ostream & out) const;
   /*----------------------------------------------------------------------
     Display a list.

     Precondition:  The ostream out is open. 
     Postcondition: The list represented by this List object has been
         inserted into out. 
   -----------------------------------------------------------------------*/

   int index( ElementType item ) const;
   /*----------------------------------------------------------------------
     Returns the position where the element item appears in the list.

     Precondition:  The list is not empty, element is in the list.
     Postcondition: Returns the first position where element item appears.
   ----------------------------------------------------------------------*/  
   int count( ElementType item ) const;
   /*----------------------------------------------------------------------
     Returns the amount of times element item appears in the list.

     Precondition:  The list is not empty.
     Postcondition: Returns the amount of times element item appears.
   ----------------------------------------------------------------------*/  

   void reverse( );
   /*----------------------------------------------------------------------
     Inverts the list

     Precondition:  The list is not empty.
     Postcondition: The list is inverted.
   ----------------------------------------------------------------------*/  

   int getSize( ){ return mySize; }

   int getElemAtPos( int pos ){ return myArray[ pos ]; }

   bool empty() const;

   void display(std::ostream & out) const;

   bool withinBounds( int pos ) const;

 private:
 /******** Data Members ********/
   int mySize;                     // current size of list stored in myArray
   ElementType myArray[CAPACITY];  // array to store list elements

}; //--- end of List class

//------ Prototype of output operator
std::ostream & operator<< (std::ostream & out, const List & aList);

void err_out(std::string const &error);

#endif
